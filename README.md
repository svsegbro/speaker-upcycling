# Speaker upcycling

Repository on upcycling a set of old ("vintage" if you prefer), passive B&O speakers.

Outline:
* A set of Beovox CX100 speakers.
* Foams were worn out and had to be re-done.
* Speakers to be used in combination with Spotify Connect.

## Foams
* Foam (& glue) suppliers:
  * Audiofriends *https://www.audiofriends.nl/foam-surrounds/*
* Manuals:
  * https://www.youtube.com/watch?v=-IvtwMbza6g 
* The result of the refoaming included in the pics.

## Amplifier selection
Hifiberry offers nice amplifiers, including software with off-the-shelf support for Spotify Connect (and other streaming services).

Two possibilities:
* The Hifiberry Amp2: https://www.hifiberry.com/shop/boards/hifiberry-amp2/
* The Hifiberry Beocreate: https://www.hifiberry.com/shop/#beocreate

## Setup using Beocreate
General instructions:
* https://www.youtube.com/watch?v=i766zXHoL1U (go to 03:12)

Amplifier mounting:
* Hifiberry suggests mounting the amplifier (and Raspberry Pi) inside one of the speakers. 
* I prefer using a separate external casing for the amp, as such approach offers more flexibility for future use. E.g. connecting the speakers to another audio source, such as a television.  

Back-plate mounting details: 
* Mount banana plugs on the back plate of the speakers.
* See pictures folder for an example case.

Wiring details:
* See: https://www.youtube.com/watch?v=i766zXHoL1U (go to 03:12)
* See: https://www.hifiberry.com/blog/stereo-setup-for-a-pair-of-cx50-cx100-speakers/

## Fabric
* https://www.hifiberry.com/blog/changing-the-spaker-fabric-of-your-beovox-cx-50-cx100/

## Drivers
Alternative to refoaming:
* http://www.visaton.net/

